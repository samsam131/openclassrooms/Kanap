let url = new URL(document.location)
let params = url.searchParams;
let id = params.get("id");

//Requête GET pour récupérer les caractéristique du produit sélectionner
fetch(`http://localhost:3000/api/products/${id}`)
    .then(function (res) {
        if (res.ok) {
            return res.json();
        }
    })
    .then(function (kanap) {
        let kanapTitle = document.querySelector("head title")
        kanapTitle.innerHTML = kanap.name;
        let kanapImage = document.querySelector(".item__img img");
        kanapImage.setAttribute("src", kanap.imageUrl);
        let kanapName = document.getElementById("title")
        kanapName.innerText = kanap.name;
        let kanapPrice = document.getElementById("price")
        kanapPrice.innerHTML = kanap.price
        let kanapDescription = document.getElementById("description")
        kanapDescription.innerText = kanap.description;

        for (let i in kanap.colors) {
            let colorChoice = document.createElement("option");
            colorChoice.setAttribute("value", kanap.colors[i]);
            colorChoice.innerHTML = kanap.colors[i];
            let selectColor = document.querySelector("#colors")
            selectColor.appendChild(colorChoice);
        }
    })

// Enregistrer les éléments au click sur le bouton dans le localStorage
const colorSelect = document.getElementById("colors");
const buttonAddToCart = document.getElementById("addToCart");
colorSelect.addEventListener("change", function (){
    const index = colorSelect.selectedIndex
if (index == 0) {
    buttonAddToCart.disabled = true

} else {
    buttonAddToCart.disabled = false




buttonAddToCart.addEventListener('click', function (event) {
    event.preventDefault
    let savedCart = localStorage.getItem("myKanap") // Récupération du conteni du localStorage
    let myCart = savedCart ? JSON.parse(savedCart) : [] // Transformation du format JSON
    let kanapColor = document.getElementById("colors").value
    let kanapNumber = parseInt(document.getElementById("quantity").value)
    let item = {
        id,
        kanapNumber,
        kanapColor
    }

    //Contrôle de la présence de l'id dans le local storage: 
    //si oui = et si même couleur : enregistrement de la quantité 
    //si non = enregistrement de la nouvelle id + couleur + quantité
    if (myCart.length > 0) {
        const existingItem = myCart.find(item => item.id == id && item.kanapColor == kanapColor)
        existingItem ? existingItem.kanapNumber += kanapNumber : myCart.push(item)
    } else {
        myCart.push(item)
    }
    localStorage.setItem("myKanap", JSON.stringify(myCart))
    let kanapName = document.getElementById("title")
    window.alert("L'article -" + kanapName.innerText + "- a bien été ajouté au panier");
    
})
}

})