/* ----- Afficher un tableau récapitulatif des achats dans la page Panier -----*/

//A partir des id récupérées dans l'array du localStorage, récpuérer les caractéristiques des produits

// Depuis la page Panier, récupérer le panier (l’array) via localStorage.
const generateCart = () => {
    const section = document.getElementById("cart__items");
    section.innerHTML = ""
    const savedCart = localStorage.getItem("myKanap")
    let myCart = savedCart ? JSON.parse(savedCart) : []


    // Parcourir l’array du localStorage
    if (myCart.length > 0) {
        let totalPrice = 0
        let totalElements = 0
        myCart.forEach((item, index) => {
            let id = item.id;
            let kanapColor = item.kanapColor
            let kanapNumber = item.kanapNumber

            //Requête sur l'API pour récupérer les caractériques produits

            const getProduct = fetch(`http://localhost:3000/api/products/${id}`)
                .then(function (res) {

                    if (res.ok) {
                        return res.json();
                    }
                })
                .then(function (kanap) {

                    // Modification du DOM pour inclure les produits 
                    const article = document.createElement("article");
                    article.classList.add("cart__item");
                    article.setAttribute("data-id", `${id}`);
                    const div1 = document.createElement("div");
                    div1.classList.add("cart__item__img");
                    const image = document.createElement("img");
                    image.setAttribute("src", kanap.imageUrl);
                    image.setAttribute("alt", kanap.altTxt);
                    const div2 = document.createElement("div");
                    div2.classList.add("cart__item__content");
                    const div3 = document.createElement("div");
                    div3.classList.add("cart__item__content__titlePrice");
                    const h2 = document.createElement("h2");
                    h2.innerText = kanap.name;
                    const p0 = document.createElement("p")
                    p0.innerText = kanapColor
                    const p1 = document.createElement("p");
                    p1.innerText = kanap.price;
                    const div4 = document.createElement("div");
                    div4.classList.add("cart__item__content__settings");
                    const div5 = document.createElement("div");
                    div5.classList.add("cart__item__content__settings__quantity");
                    const p2 = document.createElement("p");
                    p2.innerText = "Quantitée = ";
                    const input = document.createElement("input");
                    input.setAttribute("type", "number");
                    input.classList.add("itemQuantity");
                    input.setAttribute("name", "itemQuantity");
                    input.setAttribute("min", 1);
                    input.setAttribute("max", 100)
                    input.setAttribute("value", kanapNumber);

                    //Modification de la quantité
                    input.addEventListener('change', (event) => {
                        item.kanapNumber = parseInt(event.target.value)
                        localStorage.setItem("myKanap", JSON.stringify(myCart))
                        generateCart()
                    })

                    const div6 = document.createElement("div");
                    div6.classList.add("cart__item__content__settings__delete");
                    const p3 = document.createElement("p");
                    p3.classList.add("deleteItem");
                    p3.textContent = "Supprimer";

                    //Suppression des articles
                    p3.addEventListener("click", () => {
                        myCart.splice(index, 1)
                        localStorage.setItem("myKanap", JSON.stringify(myCart))
                        generateCart()
                    })

                    section.appendChild(article);
                    article.appendChild(div1);
                    article.appendChild(div2);
                    div1.appendChild(image);
                    div2.appendChild(div3);
                    div2.appendChild(div4);
                    div4.appendChild(div5);
                    div4.appendChild(div6);
                    div3.appendChild(h2);
                    div3.appendChild(p0)
                    div3.appendChild(p1);
                    div5.appendChild(p2);
                    div5.appendChild(input);
                    div6.appendChild(p3);

                    //Calcul de la qté et prix total
                    totalElements += item.kanapNumber
                    totalPrice += (item.kanapNumber * kanap.price)
                    const totalQuantity = document.getElementById("totalQuantity")
                    const kanapsPrice = document.getElementById("totalPrice")
                    totalQuantity.innerHTML = totalElements
                    kanapsPrice.innerHTML = totalPrice
                })
        })
    }
}
generateCart()