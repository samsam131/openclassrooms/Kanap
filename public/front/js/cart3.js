// Vérification des champs avec les RegEx

const order = document.getElementById("order")

//Prénom
let firstNameContact = document.getElementById("firstName")
firstNameContact.addEventListener("focusout", (event) => {
    let nameRegExp = new RegExp('^[A-zÀ-ÿ-]+[A-zÀ-ÿ-]$', 'g', )
    let testName = nameRegExp.test(firstNameContact.value)
    let nameMsgErr = firstNameContact.nextElementSibling
    if (event.target.value === ""){
        nameMsgErr.innerText = "Veuillez renseigner un prénom"
        order.disabled = true
    } else if (testName) {
        nameMsgErr.innerText = "Préom valide"
    } else {
        nameMsgErr.innerText = "Veuillez entrer un prénom valide"
        order.disabled = true
    }
})

//Nom
let lastNameContact = document.getElementById("lastName")
lastNameContact.addEventListener("focusout", (event) => {
    let nameRegExp = new RegExp('^[A-zÀ-ÿ-]+[A-zÀ-ÿ-]$', 'g', )
    let testName = nameRegExp.test(lastNameContact.value)
    let nameMsgErr = lastNameContact.nextElementSibling
    if (event.target.value === ""){
        nameMsgErr.innerText = "Veuillez renseigner un nom"
        order.disabled = true
    } else if (testName) {
        nameMsgErr.innerText = "Nom valide"
    } else {
        nameMsgErr.innerText = "Veuillez entrer un nom valide"
        order.disabled = true
    }
})

// Adresse
let addressContact = document.getElementById("address")
addressContact.addEventListener("focusout", (event) => {
    //const testAddress = addressValidator(event.target.value)
    let addressRegExp = new RegExp('^[A-zÀ-ÿ0-9 .-]+[A-zÀ-ÿ0-9 .-]$', 'g', ) //('[0-9]*) ?([a-zA-Z,\. ]*', 'g')
    let testAddress = addressRegExp.test(addressContact.value)
    let addressMsgErr = addressContact.nextElementSibling
    if (event.target.value === ""){
        addressMsgErr.innerText = "Veuillez renseigner une adresse"
        order.disabled = true
    } else if (testAddress) {
        addressMsgErr.innerText = "Adresse valide"
    } else {
        addressMsgErr.innerText = "Veuillez entrer une adresse valide"
        order.disabled = true
    }
})

//Ville
let cityContact = document.getElementById("city")
cityContact.addEventListener("focusout", (event) => {
    let cityRegExp = new RegExp('^[A-zÀ-ÿ-]+[A-zÀ-ÿ-]$', 'g', )
    let testCity = cityRegExp.test(cityContact.value)
    let cityMsgErr = cityContact.nextElementSibling
    if (event.target.value === ""){
        cityMsgErr.innerText = "Veuillez renseigner une ville"
        order.disabled = true
    } else if (testCity) {
        cityMsgErr.innerText = "Ville valide"
    } else {
        cityMsgErr.innerText = "Veuillez entrer une ville valide"
        order.disabled = true
    }
})
//Email
let emailContact = document.getElementById("email")
emailContact.addEventListener("focusout", (event) => {
        const testEmail = validator.isEmail(event.target.value)
        let emailMsgErr = emailContact.nextElementSibling
        if (event.target.value === ""){
            emailMsgErr.innerText = "Veuillez renseigner une adresse mail"
        } else if (testEmail) {
            emailMsgErr.innerText = "Adresse mail valide"
        } else {
            emailMsgErr.innerText = "Veuillez entrer une adresse mail valide"
            order.disabled = true
        }
    }

)
//Création objet contact
let myContact
const crateContact = () => {
    let firstNameContact2 = firstNameContact.value
    let lastNameContact2 = lastNameContact.value
    let addressContact2 = addressContact.value
    let cityContact2 = cityContact.value
    let emailContact2 = emailContact.value

    class contact {
        constructor(firstName, lastName, address, city, email) {
            this.firstName = firstName,
                this.lastName = lastName,
                this.address = address,
                this.city = city,
                this.email = email
        }
    }
    myContact = new contact(firstNameContact2, lastNameContact2, addressContact2, cityContact2, emailContact2)
}

createOrder = () => {
    //Création de l'objet product
    const savedCart = localStorage.getItem("myKanap")
    let myCart = savedCart ? JSON.parse(savedCart) : []
    const products = []
    myCart.forEach((item) => {
        products.push(item.id)
    })

    //Création du Body de la requête POST
    const bodyProduct = JSON.stringify(products)
    const bodyContact = JSON.stringify(myContact)
    const body = JSON.stringify({
        "contact": myContact,
        "products": products
    })

    //Requête POST à l'API pour répurer le n° de commande
    fetch("http://localhost:3000/api/products/order", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: body
        })
        .then(function (res) {
            
            if (res.ok) {
                return res.json();
            }
        })
        .then(function (result) {
            
            const orderNumber = result.orderId
            document.location.href = ("./confirmation.html?orderId=" + orderNumber)

        })

}
//Passage de la commande au click sur le bouton
order.addEventListener("click", function (e) {
    e.preventDefault()
    crateContact()
    createOrder()
    localStorage.removeItem("myKanap")
})