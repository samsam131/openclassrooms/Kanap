// Requête de l'API pour obtenir l'ensemble des produits
fetch("http://localhost:3000/api/products")
    .then(function (res) {
        if (res.ok) {
            return res.json();
        }
    })
    // Modification du DOM pour inclure les produits     
    .then(function (kanaps) {
        const itemsContainer = document.getElementById('items')
        kanaps.forEach(kanap => {
            const a = document.createElement("a")
            a.setAttribute("href", "./product.html?id=" + kanap._id)
            const article = document.createElement("article")
            const image = document.createElement("img")
            image.setAttribute("src", kanap.imageUrl)
            image.setAttribute("alt", kanap.altTxt)
            const h3 = document.createElement("h3")
            h3.classList.add("productName")
            h3.innerText = kanap.name
            const p = document.createElement("p")
            p.classList.add("productDescription")
            p.innerText = kanap.description
            article.appendChild(image)
            article.appendChild(h3)
            article.appendChild(p)
            a.appendChild(article)
            itemsContainer.appendChild(a)
        });
    })
    .catch(function (err) {
        console.error(err)
    });